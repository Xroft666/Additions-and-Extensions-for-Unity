﻿using System;
using UnityEngine;
using UnityEditor;
using Hargrim.Collections;
using HargrimEditor.Windows;

namespace HargrimEditor.Drawers
{
    [CustomPropertyDrawer(typeof(BitMaskEnumFlagsAttribute))]
    internal class BitMaskEnumFlagsDrawer : PropertyDrawer
    {
        private class Data
        {
            private readonly string m_error;

            private readonly GUIContent m_label;
            private readonly string[] m_names;
            private readonly bool m_array;

            private EnumBitArrayMaskWindow m_win;

            public Data(PropertyAttribute attribute, SerializedProperty property)
            {
                Array values;

                if (property.type == typeof(BitArrayMask).Name)
                {
                    m_label = new GUIContent(property.displayName);
                    values = Enum.GetValues((attribute as BitMaskEnumFlagsAttribute).EnumType);
                    m_array = true;
                }
                else if (property.type == typeof(BitMask).Name)
                {
                    values = Enum.GetValues((attribute as BitMaskEnumFlagsAttribute).EnumType);

                    if (values.Length > BitMask.SIZE)
                    {
                        m_error = "Enum values amount cannot be more than " + BitMask.SIZE.ToString();
                        return;
                    }

                    m_label = new GUIContent(property.displayName);
                }
                else
                {
                    m_error = "Serialized field must have type of BitMask or BitArrayMask.";
                    return;
                }

                Enum lastElement = values.GetValue(values.Length - 1) as Enum;
                m_names = new string[lastElement.ToInteger() + 1];

                foreach (Enum item in values)
                {
                    m_names[item.ToInteger()] = item.GetName();
                }

                if (m_array)
                {
                    EnumBitArrayMaskWindow.CheckArray(property, m_names);
                    property.serializedObject.ApplyModifiedProperties();
                }
            }

            public void Draw(Rect position, SerializedProperty property)
            {
                if (m_error != null)
                {
                    EditorGUI.HelpBox(position, m_error, MessageType.Error);
                    return;
                }

                if (m_array)
                    BitArrayMaskDrawer.Draw(position, m_label, ref m_win, Tuple.Create(property, m_names));
                else
                {
                    SerializedProperty field = property.FindPropertyRelative(BitMask.SerFieldName);
                    field.intValue = EditorGUI.MaskField(position, m_label, field.intValue, m_names);
                }
            }
        }

        private Data m_data;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (m_data == null)
                m_data = new Data(attribute, property);

            m_data.Draw(position, property);
        }
    }
}
