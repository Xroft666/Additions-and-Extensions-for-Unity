﻿using UnityEngine;
using UnityEditor;
using Hargrim.Collections;

namespace HargrimEditor.Drawers
{
    [CustomPropertyDrawer(typeof(BitMask))]
    internal class BitMaskDrawer : PropertyDrawer
    {
        private string[] m_names;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (m_names == null)
            {
                m_names = new string[BitMask.SIZE];
                for (int i = 0; i < m_names.Length; i++)
                    m_names[i] = i.ToString();
            }

            SerializedProperty field = property.FindPropertyRelative(BitMask.SerFieldName);
            field.intValue = EditorGUI.MaskField(position, property.displayName, field.intValue, m_names);
        }
    }
}
