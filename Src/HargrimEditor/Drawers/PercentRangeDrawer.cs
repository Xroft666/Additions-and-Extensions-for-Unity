﻿using Hargrim;
using UnityEditor;
using UnityEngine;

namespace HargrimEditor.Drawers
{
    [CustomPropertyDrawer(typeof(PercentRangeAttribute))]
    internal class PercentRangeDrawer : PropertyDrawer
    {
        private string m_name;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (m_name == null)
            {
                if (property.propertyType != SerializedPropertyType.Generic || property.type != "Percent")
                {
                    EditorGUI.LabelField(position, "Serialized field must have type of Percent.");
                    return;
                }

                m_name = property.displayName + " (%)";
            }

            PercentRangeAttribute a = attribute as PercentRangeAttribute;
            SerializedProperty field = property.FindPropertyRelative(Percent.SerFieldName);
            field.floatValue = EditorGUI.Slider(position, m_name, field.floatValue / Percent.PERCENT_2_RATIO, a.Min, a.Max) * Percent.PERCENT_2_RATIO;
        }
    }
}
