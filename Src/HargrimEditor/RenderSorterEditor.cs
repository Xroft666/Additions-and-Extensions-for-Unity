﻿using UnityEngine;
using UnityEditor;
using Hargrim.Scripts;
using Hargrim;

namespace HargrimEditor
{
    [CustomEditor(typeof(RenderSorter)), CanEditMultipleObjects]
    internal class RenderSorterEditor : Editor
    {
        private SerializedProperty m_renderer;
        private SerializedProperty m_sortingLayer;
        private SerializedProperty m_sortingOrder;

        private void Awake()
        {
            m_renderer = serializedObject.FindProperty("_renderer");
            m_sortingLayer = serializedObject.FindProperty("_sortingLayer");
            m_sortingOrder = serializedObject.FindProperty("_sortingOrder");

            var r = f_checkRenderer();
            if (r != null)
                serializedObject.ApplyModifiedProperties();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            Renderer r = f_checkRenderer();

            if (r == null) { return; }

            r.sortingLayerID = m_sortingLayer.intValue;
            r.sortingOrder = m_sortingOrder.intValue;

            if (GUI.changed)
                serializedObject.ApplyModifiedProperties();
        }

        private Renderer f_checkRenderer()
        {
            Renderer r = m_renderer.objectReferenceValue as Renderer;

            if (r == null)
            {
                r = (target as RenderSorter).GetComponent<Renderer>();
                if (r != null)
                    m_renderer.objectReferenceValue = r;
                else
                    Msg.Warning("Render component is not found.");
            }

            return r;
        }
    }
}
