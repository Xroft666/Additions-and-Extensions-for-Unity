﻿using Hargrim.Sound.SoundProviderStuff;
using UnityEditor;

namespace HargrimEditor.SoundEditors
{
    [CustomEditor(typeof(MusObject))]
    internal class MusObjectEditor : SoundObjectEditor { }
}
