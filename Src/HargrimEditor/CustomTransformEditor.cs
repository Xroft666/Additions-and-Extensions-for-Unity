﻿using UnityEngine;
using UnityEditor;

namespace HargrimEditor
{
    [CustomEditor(typeof(Transform))]
    internal class CustomTransformEditor : Editor
    {
        private Transform m_target;

        void Awake()
        {
            m_target = target as Transform;
        }

        public override void OnInspectorGUI()
        {
            GUILayout.Space(5f);

            Vector3 pos, rot, scl;

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Pos", GUILayout.Width(35f))) { pos = Vector3.zero; }
            else { pos = EditorGUILayout.Vector3Field(GUIContent.none, m_target.localPosition); }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Rot", GUILayout.Width(35f))) { rot = Vector3.zero; }
            else { rot = EditorGUILayout.Vector3Field(GUIContent.none, m_target.localEulerAngles); }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Scl", GUILayout.Width(35f))) { scl = Vector3.one; }
            else { scl = EditorGUILayout.Vector3Field(GUIContent.none, m_target.localScale); }
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(5f);

            if (GUI.changed)
            {
                Undo.RecordObject(m_target, "Transform");

                m_target.localPosition = pos;
                m_target.localEulerAngles = rot;
                m_target.localScale = scl;
            }
        }
    }
}