﻿using System;
using UnityEngine;
using System.Linq;
using Hargrim.MathExt;

namespace HargrimEditor.Windows
{
    internal class CreateFiguredPlaneWizard : CreateMeshWizard
    {
        private enum Finding : byte { XY, ZX }

        [SerializeField]
        private int _vertices = 3;
        [SerializeField]
        private float _radius = 0.5f;
        [SerializeField]
        private Finding _orientation = Finding.XY;

        private void Update()
        {
            _vertices = _vertices.CutBefore(3);
        }

        protected override string GenerateObjectName()
        {
            return "Plane";
        }

        protected override string GenerateMeshName()
        {
            return string.Concat(GenerateObjectName(),
                                 _orientation.GetName(),
                                 _vertices.ToString(), "v",
                                 _radius.ToString(), "r");
        }

        protected override Mesh CreateMesh()
        {
            Mesh mesh = new Mesh
            {
                vertices = f_getVertices(),
                triangles = f_getTriangles(),
                uv = f_getUV()
            };

            mesh.RecalculateNormals();

            return mesh;
        }

        // -- //

        private Vector3[] f_getVertices()
        {
            Vector3[] vertices = new Vector3[_vertices + 1];

            for (int i = 0; i < _vertices; i++)
            {
                float angle = (360f / _vertices * i).ToRadians();

                vertices[i] = new Vector3((float)Math.Sin(angle), (float)Math.Cos(angle)) * _radius;
            }

            if (_orientation == Finding.ZX)
            {
                vertices = vertices.Select(itm => ((Vector2)itm).To_XyZ()).ToArray();
            }

            vertices[_vertices] = Vector3.zero;

            return vertices;
        }

        private int[] f_getTriangles()
        {
            int[] triangles = new int[_vertices * 3];

            for (int i = 0; i < _vertices; i++)
            {
                int trIndex = i * 3;

                triangles[trIndex] = _vertices;
                triangles[++trIndex] = i;
                triangles[++trIndex] = (i + 1) % _vertices;
            }

            return triangles;
        }

        private Vector2[] f_getUV()
        {
            Vector2[] uv = new Vector2[_vertices + 1];

            Vector2 shift = Vector2.one * 0.5f;

            uv[_vertices] = shift;

            for (int i = 0; i < _vertices; i++)
            {
                float angle = (360f / _vertices * i).ToRadians();
                uv[i] = new Vector2((float)Math.Sin(angle), (float)Math.Cos(angle)) * 0.5f + shift;
            }

            return uv;
        }
    }
}
