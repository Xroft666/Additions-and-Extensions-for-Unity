﻿using System;
using System.Linq;
using UnityEngine;

namespace HargrimEditor.Windows
{
    internal class CreateRectPlaneWizard : CreateMeshWizard
    {
        private enum Finding : byte { XY, ZX }

        [SerializeField]
        private Vector2 _size = Vector2.one;
        [SerializeField]
        private Vector2 _pivot = new Vector2(0.5f, 0.5f);
        [SerializeField]
        private Finding _orientation = Finding.XY;
        [SerializeField]
        private Rect _textureCoords = Rect.MinMaxRect(0f, 0f, 1f, 1f);

        protected override string GenerateObjectName()
        {
            return "Plane";
        }

        protected override string GenerateMeshName()
        {
            Vector2 offset = _textureCoords.position;
            Vector2 tiling = _textureCoords.size;

            return string.Concat(GenerateObjectName(), _orientation.GetName(),
                                 _size.x.ToString(), "x", _size.y.ToString(),
                                 "p", _pivot.x.ToString(), "x", _pivot.y.ToString(),
                                 "o", offset.x.ToString(), "x", offset.y.ToString(),
                                 "t", tiling.x.ToString(), "x", tiling.y.ToString());
        }

        protected override Mesh CreateMesh()
        {
            Vector3 shift = Vector2.Scale(_pivot, _size);

            Vector3[] newVertices = new[]
            {
                new Vector3(0f, 0f),
                new Vector3(_size.x, 0f),
                new Vector3(0f, _size.y),
                new Vector3(_size.x, _size.y)
            };

            if (_orientation == Finding.ZX)
            {
                newVertices = newVertices.Select(itm => ((Vector2)itm).To_XyZ()).ToArray();
                shift.Set(shift.x, 0f, shift.y);
            }

            for (int i = 0; i < newVertices.Length; i++)
            {
                newVertices[i] -= shift;
            }

            Vector2[] newUv = new[]
            {
                new Vector2(_textureCoords.xMin, _textureCoords.yMin),
                new Vector2(_textureCoords.xMax, _textureCoords.yMin),
                new Vector2(_textureCoords.xMin, _textureCoords.yMax),
                new Vector2(_textureCoords.xMax, _textureCoords.yMax)
            };

            int[] newTriangles = new[]
            {
                0, 2, 3,
                0, 3, 1
            };

            var mesh = new Mesh
            {
                vertices = newVertices,
                uv = newUv,
                triangles = newTriangles
            };

            mesh.RecalculateNormals();

            return mesh;
        }
    }
}
