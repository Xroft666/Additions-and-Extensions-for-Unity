﻿using Hargrim.MathExt;
using System;
using UnityEngine;

namespace HargrimEditor.Windows
{
    internal class CreateShapeWizard : CreateMeshWizard
    {
        [SerializeField]
        private int _edges = 3;
        [SerializeField]
        private float _pivot = 0.5f;
        [SerializeField]
        private float _topRadius = 0.5f;
        [SerializeField]
        private float _bottomRadius = 0.5f;
        [SerializeField]
        private float _height = 1f;
        [SerializeField]
        private bool _smooth;
        [SerializeField]
        private bool _tile;

        private void Update()
        {
            _edges = _edges.CutBefore(3);
            _pivot = _pivot.Saturate();
            _topRadius = _topRadius.CutBefore(0f);
            _bottomRadius = _bottomRadius.CutBefore(_topRadius);
            _height = _height.CutBefore(0f);
        }

        protected override string GenerateObjectName()
        {
            return _topRadius < _bottomRadius ? "Pyramid" : "Prism";
        }

        protected override string GenerateMeshName()
        {
            return string.Concat(GenerateObjectName(),
                                 _edges.ToString(), "e",
                                 _pivot.ToString(), "p",
                                 _topRadius.ToString(), "tr",
                                 _bottomRadius.ToString(), "br",
                                 _height.ToString(), "h",
                                 _smooth ? "_s" : string.Empty,
                                 _tile ? "_t" : string.Empty);
        }

        protected override Mesh CreateMesh()
        {
            var newVertices = f_getVertices();
            var newTriangles = f_getTriangles(newVertices.Length);
            var newUV = f_getUV(newVertices.Length);

            Mesh mesh = new Mesh
            {
                vertices = newVertices,
                triangles = newTriangles,
                uv = newUV
            };

            if (_smooth) { mesh.normals = f_getNormals(newVertices); }
            else { mesh.RecalculateNormals(); }

            return mesh;
        }

        private Vector3[] f_getVertices()
        {
            Vector3[] vertices;

            Vector3 heightVector = new Vector3(0f, _height, 0f);

            if (_topRadius.Approx(0f))
            {
                vertices = new Vector3[_edges * 4 + 1];

                for (int i = 0; i < _edges; i++)
                {
                    float angle = (360f / _edges * i).ToRadians();

                    vertices[i] = new Vector3((float)Math.Sin(-angle), 0f, (float)Math.Cos(-angle)) * _bottomRadius - heightVector * _pivot;

                    vertices[i + _edges] = heightVector * (1f - _pivot);

                    Vector3 vertPos = new Vector3((float)Math.Sin(angle), 0f, (float)Math.Cos(angle)) * _bottomRadius - heightVector * _pivot;
                    vertices[i + _edges * 2] = vertPos;
                    vertices[i + _edges * 3] = vertPos;
                }

                vertices[vertices.Length - 1] = -heightVector * _pivot;
            }
            else
            {
                vertices = new Vector3[_edges * 6 + 2];

                for (int i = 0; i < _edges; i++)
                {
                    float angle = (360f / _edges * i).ToRadians();

                    Vector3 vertexPos = new Vector3((float)Math.Sin(angle), 0f, (float)Math.Cos(angle)) * _topRadius + heightVector * (1f - _pivot);

                    vertices[i] = vertexPos;
                    vertices[i + _edges * 2] = vertexPos;
                    vertices[i + _edges * 3] = vertexPos;

                    vertices[i + _edges] = new Vector3((float)Math.Sin(-angle), 0f, (float)Math.Cos(-angle)) * _bottomRadius - heightVector * _pivot;

                    vertexPos = new Vector3((float)Math.Sin(angle), 0f, (float)Math.Cos(angle)) * _bottomRadius - heightVector * _pivot;

                    vertices[i + _edges * 4] = vertexPos;
                    vertices[i + _edges * 5] = vertexPos;
                }

                vertices[vertices.Length - 2] = heightVector * (1f - _pivot);
                vertices[vertices.Length - 1] = -heightVector * _pivot;
            }

            return vertices;
        }

        private int[] f_getTriangles(int vertices)
        {
            int[] triangles = null;

            int step = 0;
            void createTriangle(int iteration, int a, int b, int c)
            {
                int trIndex = (_edges * step + iteration) * 3;

                triangles[trIndex] = a;
                triangles[++trIndex] = b;
                triangles[++trIndex] = c;

                step++;
            }

            if (_topRadius.Approx(0f))
            {
                triangles = new int[_edges * 6];

                for (int i = 0; i < _edges; i++)
                {
                    createTriangle(i, vertices - 1, i, (i + 1) % _edges);
                    createTriangle(i, i + _edges, i + _edges * 2, (i + 1) % _edges + _edges * 3);
                    step = 0;
                }
            }
            else
            {
                triangles = new int[_edges * 12];

                for (int i = 0; i < _edges; i++)
                {
                    createTriangle(i, vertices - 2, i, (i + 1) % _edges);
                    createTriangle(i, vertices - 1, i + _edges, (i + 1) % _edges + _edges);
                    createTriangle(i, i + _edges * 2, i + _edges * 4, (i + 1) % _edges + _edges * 5);
                    createTriangle(i, (i + 1) % _edges + _edges * 5, (i + 1) % _edges + _edges * 3, i + _edges * 2);
                    step = 0;
                }
            }

            return triangles;
        }

        private Vector2[] f_getUV(int vertices)
        {
            Vector2[] uv = new Vector2[vertices];

            Vector2 shift = Vector2.one * 0.5f;

            if (_topRadius.Approx(0f))
            {
                uv[uv.Length - 1] = shift;

                for (int i = 0; i < _edges; i++)
                {
                    float angle = (360f / _edges * i).ToRadians();
                    Vector2 pos = new Vector2((float)Math.Sin(angle), (float)Math.Cos(angle)) * 0.5f + shift;
                    uv[i] = pos;
                    uv[i + _edges] = pos;

                    if (_tile)
                    {
                        uv[i + _edges] = new Vector2(0.5f, 1f);
                        uv[i + _edges * 2] = new Vector2(1f, 0f);
                        uv[i + _edges * 3] = new Vector2(0f, 0f);
                    }
                    else
                    {
                        float ratio = 1f / _edges;
                        float iRatio = 1f - ratio * i;

                        uv[i + _edges] = new Vector2(0.5f, 1f);
                        uv[i + _edges * 2] = new Vector2(iRatio, 0f);
                        uv[(i + 1) % _edges + _edges * 3] = new Vector2(iRatio - ratio, 0f);
                    }
                }
            }
            else
            {
                uv[uv.Length - 2] = shift;
                uv[uv.Length - 1] = shift;

                for (int i = 0; i < _edges; i++)
                {
                    float angle = (360f / _edges * i).ToRadians();
                    Vector2 pos = new Vector2((float)Math.Sin(angle), (float)Math.Cos(angle)) * 0.5f + shift;
                    uv[i] = pos;
                    uv[i + _edges] = pos;

                    if (_tile)
                    {
                        uv[i + _edges * 2] = new Vector2(1f, 1f);
                        uv[i + _edges * 3] = new Vector2(0f, 1f);
                        uv[i + _edges * 4] = new Vector2(1f, 0f);
                        uv[i + _edges * 5] = new Vector2(0f, 0f);
                    }
                    else
                    {
                        float ratio = 1f / _edges;
                        float iRatio = 1f - ratio * i;

                        uv[i + _edges * 2] = new Vector2(iRatio, 1f);
                        uv[(i + 1) % _edges + _edges * 3] = new Vector2(iRatio - ratio, 1f);
                        uv[i + _edges * 4] = new Vector2(iRatio, 0f);
                        uv[(i + 1) % _edges + _edges * 5] = new Vector2(iRatio - ratio, 0f);
                    }
                }
            }

            return uv;
        }

        private Vector3[] f_getNormals(Vector3[] vertices)
        {
            int len = vertices.Length;
            Vector3[] normals = new Vector3[len];

            if (_topRadius.Approx(0f))
            {
                normals[len - 1] = Vector3.down;

                for (int i = 0; i < _edges; i++)
                {
                    normals[i] = Vector3.down;

                    Vector3 side = (vertices[i + _edges * 3] - vertices[len - 1]).normalized;
                    Vector3 orthoSide = Vector3.Cross(side, Vector3.up);
                    Vector3 slopeDir = (vertices[i + _edges * 3] - vertices[i + _edges]).normalized;
                    Vector3 normal = Vector3.Cross(orthoSide, slopeDir);

                    normals[i + _edges] = normal;
                    normals[i + _edges * 2] = normal;
                    normals[i + _edges * 3] = normal;
                }
            }
            else
            {
                normals[len - 2] = Vector3.up;
                normals[len - 1] = Vector3.down;

                for (int i = 0; i < _edges; i++)
                {
                    normals[i] = Vector3.up;
                    normals[i + _edges] = Vector3.down;

                    Vector3 side = (vertices[i + _edges * 4] - vertices[len - 1]).normalized;
                    Vector3 orthoSide = Vector3.Cross(side, Vector3.up);
                    Vector3 slopeDir = (vertices[i + _edges * 4] - vertices[i + _edges * 2]).normalized;
                    Vector3 normal = Vector3.Cross(orthoSide, slopeDir);

                    normals[i + _edges * 2] = normal;
                    normals[i + _edges * 3] = normal;
                    normals[i + _edges * 4] = normal;
                    normals[i + _edges * 5] = normal;
                }
            }

            return normals;
        }
    }
}
