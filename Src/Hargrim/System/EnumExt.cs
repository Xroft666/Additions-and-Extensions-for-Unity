﻿using System;

namespace Hargrim
{
    public static class EnumExt<TEnum> where TEnum : Enum
    {
        public static readonly int Count;

        static EnumExt()
        {
            Count = EnumExt.GetNames<TEnum>().Length;
        }
    }

    public static class EnumExt
    {
        public static string[] GetNames<TEnum>() where TEnum : Enum
        {
            return Enum.GetNames(typeof(TEnum));
        }

        public static TEnum[] GetValues<TEnum>() where TEnum : Enum
        {
            return Enum.GetValues(typeof(TEnum)) as TEnum[];
        }
    }
}
