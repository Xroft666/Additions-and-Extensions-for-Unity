﻿namespace System.Collections.Unsafe
{
    public static class StackArray
    {
        public static unsafe void Sort(int* array, int length)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array), "Pointer cannot be null.");

            ArrayHelper.QuickSort(array, 0, length - 1);
        }

        public static unsafe void Sort(float* array, int length)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array), "Pointer cannot be null.");

            ArrayHelper.QuickSort(array, 0, length - 1);
        }

        public static unsafe int Sum(int* array, int length)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array), "Pointer cannot be null.");

            int sum = 0;

            for (int i = 0; i < length; i++)
            {
                sum += array[i];
            }

            return sum;
        }

        public static unsafe float Sum(float* array, int length)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array), "Pointer cannot be null.");

            float sum = 0;

            for (int i = 0; i < length; i++)
            {
                sum += array[i];
            }

            return sum;
        }

        public static unsafe int Min(int* array, int length)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array), "Pointer cannot be null.");

            if (length <= 0)
                throw new InvalidOperationException("Array is empty.");

            int num = array[0];

            for (int i = 1; i < length; i++)
            {
                if (array[i] < num)
                    num = array[i];
            }

            return num;
        }

        public static unsafe float Min(float* array, int length)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array), "Pointer cannot be null.");

            if (length <= 0)
                throw new InvalidOperationException("Array is empty.");

            float num = array[0];

            for (int i = 1; i < length; i++)
            {
                if (array[i] < num)
                    num = array[i];
            }

            return num;
        }

        public static unsafe int Max(int* array, int length)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array), "Pointer cannot be null.");

            if (length <= 0)
                throw new InvalidOperationException("Array is empty.");

            int num = array[0];

            for (int i = 1; i < length; i++)
            {
                if (array[i] > num)
                    num = array[i];
            }

            return num;
        }

        public static unsafe float Max(float* array, int length)
        {
            if (array == null)
                throw new ArgumentNullException(nameof(array), "Pointer cannot be null.");

            if (length <= 0)
                throw new InvalidOperationException("Array is empty.");

            float num = array[0];

            for (int i = 1; i < length; i++)
            {
                if (array[i] > num)
                    num = array[i];
            }

            return num;
        }
    }
}
