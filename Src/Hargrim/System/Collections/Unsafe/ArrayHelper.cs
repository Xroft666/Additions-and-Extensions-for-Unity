﻿namespace System.Collections.Unsafe
{
    internal static class ArrayHelper
    {
        public static unsafe void QuickSort(int* array, int left, int right)
        {
            int i = left, j = right;
            int pivot = array[(left + right) / 2];

            while (i <= j)
            {
                while (array[i] < pivot) { i++; }
                while (array[j] > pivot) { j--; }

                if (i <= j)
                {
                    int tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;

                    i++;
                    j--;
                }
            }

            if (left < j)
                QuickSort(array, left, j);

            if (i < right)
                QuickSort(array, i, right);
        }

        public static unsafe void QuickSort(float* array, int left, int right)
        {
            int i = left, j = right;
            float pivot = array[(left + right) / 2];

            while (i <= j)
            {
                while (array[i] < pivot) { i++; }
                while (array[j] > pivot) { j--; }

                if (i <= j)
                {
                    float tmp = array[i];
                    array[i] = array[j];
                    array[j] = tmp;

                    i++;
                    j--;
                }
            }

            if (left < j)
                QuickSort(array, left, j);

            if (i < right)
                QuickSort(array, i, right);
        }

        // -- //

        //public static TSource ValueMin<TSource, TKey>(IList<TSource> collection, Func<TSource, TKey> keySelector) where TKey : IComparable<TKey>
        //{
        //    TKey min = keySelector(collection[0]);
        //    TSource element = collection[0];

        //    int count = collection.Count;

        //    for (int i = 1; i < count; i++)
        //    {
        //        TKey key = keySelector(collection[i]);

        //        if (key.CompareTo(min) < 0)
        //        {
        //            min = key;
        //            element = collection[i];
        //        }
        //    }

        //    return element;
        //}

        //public static TSource ValueMax<TSource, TKey>(IList<TSource> collection, Func<TSource, TKey> keySelector) where TKey : IComparable<TKey>
        //{
        //    TKey max = keySelector(collection[0]);
        //    TSource element = collection[0];

        //    int count = collection.Count;

        //    for (int i = 1; i < count; i++)
        //    {
        //        TKey key = keySelector(collection[i]);

        //        if (key.CompareTo(max) > 0)
        //        {
        //            max = key;
        //            element = collection[i];
        //        }
        //    }

        //    return element;
        //}
    }
}
