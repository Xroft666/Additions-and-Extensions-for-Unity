﻿namespace System.Collections.Generic
{
    internal static class CollectionHelper
    {
        public static TSource ValueMin<TSource, TKey>(IList<TSource> collection, Func<TSource, TKey> keySelector) where TKey : IComparable<TKey>
        {
            TKey min = keySelector(collection[0]);
            TSource element = collection[0];

            int count = collection.Count;

            for (int i = 1; i < count; i++)
            {
                TKey key = keySelector(collection[i]);

                if (key.CompareTo(min) < 0)
                {
                    min = key;
                    element = collection[i];
                }
            }

            return element;
        }

        public static TSource ValueMax<TSource, TKey>(IList<TSource> collection, Func<TSource, TKey> keySelector) where TKey : IComparable<TKey>
        {
            TKey max = keySelector(collection[0]);
            TSource element = collection[0];

            int count = collection.Count;

            for (int i = 1; i < count; i++)
            {
                TKey key = keySelector(collection[i]);

                if (key.CompareTo(max) > 0)
                {
                    max = key;
                    element = collection[i];
                }
            }

            return element;
        }

        public static TSource RefMin<TSource, TKey>(IList<TSource> collection, Func<TSource, TKey> keySelector) where TKey : IComparable<TKey>
        {
            TKey min = keySelector(collection[0]);
            TSource element = collection[0];

            int count = collection.Count;

            for (int i = 1; i < count; i++)
            {
                TKey key = keySelector(collection[i]);

                if (Compare(key, min) < 0)
                {
                    min = key;
                    element = collection[i];
                }
            }

            return element;
        }

        public static TSource RefMax<TSource, TKey>(IList<TSource> collection, Func<TSource, TKey> keySelector) where TKey : IComparable<TKey>
        {
            TKey max = keySelector(collection[0]);
            TSource element = collection[0];

            int count = collection.Count;

            for (int i = 1; i < count; i++)
            {
                TKey key = keySelector(collection[i]);

                if (Compare(key, max) > 0)
                {
                    max = key;
                    element = collection[i];
                }
            }

            return element;
        }

        public static int Compare<T>(T a, T b) where T : IComparable<T>
        {
            if (a != null)
                return a.CompareTo(b);
            if (b != null)
                return -b.CompareTo(a);
            return 0;
        }
    }
}
