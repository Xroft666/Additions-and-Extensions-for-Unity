﻿using Hargrim;
using System.Collections.ObjectModel;

namespace System.Collections.Generic
{
    public static class CollectionExtensions
    {
        /// <summary>
        /// Shuffles the elements of an entire collection.
        /// </summary>
        public static void Shuffle<T>(this IList<T> list)
        {
            int last = list.Count;

            while (last > 1)
            {
                int cur = Rnd.Random(last--);

                T value = list[cur];
                list[cur] = list[last];
                list[last] = value;
            }
        }

        /// <summary>
        /// Gets an index of the first entrance of the specified element.
        /// Returns index or -1 if element is not found.
        /// </summary>
        public static int IndexOf<T>(this T[] array, T item)
        {
            return Array.IndexOf(array, item);
        }

        /// <summary>
        /// Gets an index of the first entrance of an element that matches the specified condition.
        /// Returns index or -1 if element is not found.
        /// </summary>
        public static int IndexOf<T>(this T[] array, Predicate<T> condition)
        {
            return Array.FindIndex(array, condition);
        }

        /// <summary>
        /// Gets an index of the first entrance of an element that matches the specified condition.
        /// Returns index or -1 if element is not found.
        /// </summary>
        public static int IndexOf<T>(this List<T> list, Predicate<T> condition)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (condition(list[i]))
                    return i;
            }

            return -1;
        }

        /// <summary>
        /// Returns an element at the last index.
        /// </summary>
        public static T GetLast<T>(this IList<T> collection)
        {
            if (collection.Count == 0)
                throw new InvalidOperationException("Collection is empty.");

            return collection[collection.Count - 1];
        }

        /// <summary>
        /// Returns an element with the minimum parameter value.
        /// </summary>
        public static TSource GetWithMin<TSource, TKey>(this IList<TSource> collection, Func<TSource, TKey> keySelector) where TKey : IComparable<TKey>
        {
            if (collection.Count == 0)
                throw new InvalidOperationException("Collection is empty.");

            if (typeof(TKey).IsValueType)
                return CollectionHelper.ValueMin(collection, keySelector);
            else
                return CollectionHelper.RefMin(collection, keySelector);
        }

        /// <summary>
        /// Returns an element with the maximum parameter value.
        /// </summary>
        public static TSource GetWithMax<TSource, TKey>(this IList<TSource> collection, Func<TSource, TKey> keySelector) where TKey : IComparable<TKey>
        {
            if (collection.Count == 0)
                throw new InvalidOperationException("Collection is empty.");

            if (typeof(TKey).IsValueType)
                return CollectionHelper.ValueMax(collection, keySelector);
            else
                return CollectionHelper.RefMax(collection, keySelector);
        }

        /// <summary>
        /// Returns subarray of the specified length starting from specified index.
        /// </summary>
        public static T[] GetSubArray<T>(this List<T> list, int startIndex, int length)
        {
            T[] subArray = new T[length];
            for (int i = 0; i < length; i++) { subArray[i] = list[i + startIndex]; }
            return subArray;
        }

        /// <summary>
        /// Returns subarray starting from specified index.
        /// </summary>
        public static T[] GetSubArray<T>(this List<T> list, int startIndex)
        {
            return GetSubArray(list, startIndex, list.Count - startIndex);
        }

        /// <summary>
        /// Returns subarray of the specified length starting from specified index.
        /// </summary>
        public static T[] GetSubArray<T>(this T[] array, int startIndex, int length)
        {
            T[] subArray = new T[length];
            Array.Copy(array, startIndex, subArray, 0, length);
            return subArray;
        }

        /// <summary>
        /// Returns subarray starting from specified index.
        /// </summary>
        public static T[] GetSubArray<T>(this T[] array, int startIndex)
        {
            return GetSubArray(array, startIndex, array.Length - startIndex);
        }

        /// <summary>
        /// Returns a copy of the array;
        /// </summary>
        public static T[] GetCopy<T>(this T[] array)
        {
            T[] copy = new T[array.Length];
            Array.Copy(array, copy, array.Length);
            return copy;
        }

        /// <summary>
        /// Sorts the elements in an entire System.Array using the System.IComparable`1 generic interface implementation of each element of the System.Array.
        /// </summary>        
        public static void Sort<T>(this T[] array) where T : IComparable<T>
        {
            Array.Sort(array);
        }

        /// <summary>
        /// Sorts by comparison.
        /// </summary>
        /// <param name="comparer">Reference to comparing function.</param>
        public static void Sort<T>(this T[] array, Comparison<T> comparer)
        {
            Array.Sort(array, comparer);
        }

        /// <summary>
        /// Sorts by selected key.
        /// </summary>                
        /// <param name="keySelector">Reference to selecting function.</param>
        public static void Sort<TSource, TKey>(this TSource[] array, Func<TSource, TKey> keySelector) where TKey : IComparable<TKey>
        {
            if (typeof(TKey).IsValueType)
                Array.Sort(array, (itm1, itm2) => keySelector(itm1).CompareTo(keySelector(itm2)));
            else
                Array.Sort(array, (itm1, itm2) => CollectionHelper.Compare(keySelector(itm1), keySelector(itm2)));
        }

        /// <summary>
        /// Sorts by selected key.
        /// </summary>                
        /// <param name="keySelector">Reference to selecting function.</param>
        public static void Sort<TSource, TKey>(this List<TSource> list, Func<TSource, TKey> keySelector) where TKey : IComparable<TKey>
        {
            if (typeof(TKey).IsValueType)
                list.Sort((itm1, itm2) => keySelector(itm1).CompareTo(keySelector(itm2)));
            else
                list.Sort((itm1, itm2) => CollectionHelper.Compare(keySelector(itm1), keySelector(itm2)));
        }

        /// <summary>
        /// Finds specified element.
        /// </summary>        
        public static T Find<T>(this T[] array, T item)
        {
            return Array.Find(array, itm => itm.Equals(item));
        }

        /// <summary>
        /// Finds an element by condition.
        /// </summary>        
        /// <param name="match">Reference to matching function.</param>
        public static T Find<T>(this T[] array, Predicate<T> match)
        {
            return Array.Find(array, match);
        }

        /// <summary>
        /// Reverses the sequence of the elements.
        /// </summary>
        public static void Reverse<T>(this T[] array)
        {
            Array.Reverse(array);
        }

        /// <summary>
        /// Reverses the sequence of the elements.
        /// </summary>
        /// <param name="index">The starting index of the section to reverse.</param>
        /// <param name="length">The number of elements in the section to reverse.</param>
        public static void Reverse<T>(this T[] array, int index, int length)
        {
            Array.Reverse(array, index, length);
        }

        /// <summary>
        /// Sets elements of the array to their default value.
        /// </summary>
        public static void Clear<T>(this T[] array)
        {
            Array.Clear(array, 0, array.Length);
        }

        /// <summary>
        /// Sets elements of the array to their default value.
        /// </summary>
        /// <param name="index">The starting index of the range of elements to clear.</param>
		/// <param name="length">The number of elements to clear.</param>
        public static void Clear<T>(this T[] array, int index, int length)
        {
            Array.Clear(array, index, length);
        }

        /// <summary>
        /// Returns new array contained elements got by converting the array.
        /// </summary>
        /// <param name="converter">Conveting function.</param>
        public static TOut[] GetConverted<TIn, TOut>(this TIn[] array, Converter<TIn, TOut> converter)
        {
            return Array.ConvertAll(array, converter);
        }

        /// <summary>
        /// Returns whether an array contains specified item.
        /// </summary>
        public static bool Contains<T>(this T[] array, T item)
        {
            return (array as IList<T>).Contains(item);
        }

        /// <summary>
        /// Removes an element at the specified index of the System.Collections.Generic.List`1 and returns that element.
        /// </summary>
        public static T PullOut<T>(this List<T> list, int index)
        {
            T item = list[index];
            list.RemoveAt(index);
            return item;
        }

        /// <summary>
        /// Adds the element of the System.Collections.Generic.List`1 and returns that element.
        /// </summary>
        public static T AddNGet<T>(this List<T> list, T newItem)
        {
            list.Add(newItem);
            return newItem;
        }

        /// <summary>
        /// Inserts an element into the System.Collections.Generic.List`1 at the specified index and returns that element.
        /// </summary>
        public static T InsertNGet<T>(this List<T> list, int index, T newItem)
        {
            list.Insert(index, newItem);
            return newItem;
        }

        /// <summary>
        /// Returns a copy of the list;
        /// </summary>
        public static List<T> GetCopy<T>(this List<T> list)
        {
            return new List<T>(list);
        }

        /// <summary>
        /// Adds the element of the System.Collections.Generic.Dictionary`2 and returns that element.
        /// </summary>
        public static TValue AddNGet<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue newItem)
        {
            dict.Add(key, newItem);
            return newItem;
        }

        public static ReadOnlyDictionary<TKey, TValue> AsReadOnly<TKey, TValue>(this Dictionary<TKey, TValue> dict)
        {
            return new ReadOnlyDictionary<TKey, TValue>(dict);
        }
    }
}
