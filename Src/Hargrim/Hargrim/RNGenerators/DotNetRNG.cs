﻿using System;

namespace Hargrim.RNGenerators
{
    public sealed class DotNetRNG : Random, RNG
    {
        public DotNetRNG() : base() { }

        public DotNetRNG(int seed) : base(seed) { }

        public float NextFloat(float minValue, float maxValue)
        {
            if (minValue > maxValue)
                throw new ArgumentOutOfRangeException(nameof(minValue), string.Format("{0} cannot be more than {1}.", nameof(minValue), nameof(maxValue)));

            return (float)(Sample() * ((double)maxValue - minValue) + minValue);
        }

        public unsafe void NextBytes(byte* arrayPtr, int length)
        {
            if (arrayPtr == null)
                throw new ArgumentNullException(nameof(arrayPtr), "Pointer cannot be null.");

            for (int i = 0; i < length; i++)
            {
                arrayPtr[i] = (byte)Next(256);
            }
        }
    }
}
