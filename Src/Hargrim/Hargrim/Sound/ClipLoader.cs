﻿using UnityEngine;

namespace Hargrim.Sound
{
    public interface ClipLoader
    {
        AudioClip LoadClip(string name);
    }
}
