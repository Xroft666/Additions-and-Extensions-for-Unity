﻿using UnityEngine;
using System.Collections;
using Hargrim.Collections;
using System;
using Hargrim.Scripts;
using Hargrim.MathExt;

namespace Hargrim.Sound.SoundProviderStuff
{
    public class MusObject : SoundObjectInfo, Poolable
    {
        [SerializeField]
        private AudioSource _audioSource;

        private float m_volume;
        private float m_pitch;
        private MusicProvider m_provider;
        private Action m_update;

        internal override string ClipName
        {
            get { return _audioSource.clip.name; }
        }

        internal override AudioSource AudioSource
        {
            get { return _audioSource; }
        }

        ///////////////
        //Unity Funcs//
        ///////////////

        private void Awake()
        {
            gameObject.Immortalize();

            if (_audioSource == null)
            {
                _audioSource = gameObject.GetOrAddComponent<AudioSource>();
                _audioSource.playOnAwake = false;
            }

            m_update = () =>
            {
                if (_audioSource.isPlaying || _audioSource.time != 0f) { return; }
                Stop();
            };

            f_init();
        }

        ////////////////
        //Public Funcs//
        ////////////////        

        internal void Play(MusicProvider provider, AudioClip clip, MPreset set)
        {
            m_provider = provider;

            _audioSource.clip = clip;
            _audioSource.loop = set.Looped;
            _audioSource.time = set.StartTime;
            m_pitch = set.Pitch;

            if (set.Rising)
            {
                m_volume = 0f;
                StartCoroutine(Rise(set));
            }
            else
            {
                m_volume = set.Volume;
                UpdVolume();
            }

            UpdPitch();
            _audioSource.PlayDelayed(set.StartDelay);
        }

        internal override void Stop()
        {
            StopAllCoroutines();
            _audioSource.Stop();
            m_provider.ReleaseMusic(this);
        }

        internal void StopFading(float time)
        {
            StopAllCoroutines();
            StartCoroutine(FadeAndStop(time));
        }

        internal void Mute(bool value)
        {
            _audioSource.mute = value;
        }

        internal void UpdVolume()
        {
            _audioSource.volume = m_volume * m_provider.Volume;
        }

        internal void UpdPitch()
        {
            _audioSource.pitch = m_pitch * m_provider.Pitch;
        }

        internal void Pause()
        {
            _audioSource.Pause();
        }

        internal void UnPause()
        {
            _audioSource.UnPause();
        }

        ///////////////
        //Inner Funcs//
        ///////////////

        private void f_init()
        {
            Updater.Frame_Event += m_update;
        }

        #region IPoolable
        void Poolable.Reinit()
        {
            gameObject.SetActive(true);
            f_init();
        }

        void Poolable.CleanUp()
        {
            gameObject.SetActive(false);
            _audioSource.clip = null;
            Updater.Frame_Event -= m_update;
        }
        #endregion

        ////////////
        //Routines//
        ////////////

        IEnumerator Rise(MPreset set)
        {
            UpdVolume();
            float curTime = 0f;

            while (curTime < set.StartDelay)
            {
                yield return null;

                if (m_provider.Paused) { continue; }

                curTime += Time.unscaledDeltaTime * _audioSource.pitch.Abs();
            }

            curTime = 0f;
            float ratio = 0f;

            do
            {
                yield return null;

                if (m_provider.Paused) { continue; }

                curTime += Time.unscaledDeltaTime * m_provider.Pitch.Abs();
                ratio = curTime / set.RisingDur;
                m_volume = Mathf.Lerp(0f, set.Volume, curTime);
                UpdVolume();

            } while (ratio < 1f);
        }

        IEnumerator FadeAndStop(float time)
        {
            if (time < 0f)
                throw new ArgumentOutOfRangeException(nameof(time), "Time cannot be negative.");

            float startVal = m_volume;
            float curTime = 0f;
            float ratio = 0f;

            do
            {
                yield return null;

                if (m_provider.Paused) { continue; }

                curTime += Time.unscaledDeltaTime * m_provider.Pitch.Abs();
                ratio = curTime / time;
                m_volume = Mathf.Lerp(startVal, 0f, ratio);
                UpdVolume();

            } while (ratio < 1f);

            _audioSource.Stop();
            m_provider.ReleaseMusic(this);
        }
    }
}
