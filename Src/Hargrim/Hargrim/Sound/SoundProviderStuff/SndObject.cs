using UnityEngine;
using System.Collections;
using Hargrim.Collections;
using Hargrim.Scripts;
using System;
using Hargrim.MathExt;

namespace Hargrim.Sound.SoundProviderStuff
{
    public class SndObject : SoundObjectInfo, Poolable
    {
        internal object Sender;

        [SerializeField]
        private AudioSource _audioSource;

        private float m_volume;
        private float m_pitch;
        private SoundProvider m_provider;
        private Action m_update;

        internal override string ClipName
        {
            get { return _audioSource.clip.name; }
        }

        internal bool IsLooped
        {
            get { return _audioSource.loop; }
        }

        internal override AudioSource AudioSource
        {
            get { return _audioSource; }
        }

        ///////////////
        //Unity Funcs//
        ///////////////

        private void Awake()
        {
            gameObject.Immortalize();

            if (_audioSource == null)
            {
                _audioSource = gameObject.GetOrAddComponent<AudioSource>();
                _audioSource.playOnAwake = false;
                _audioSource.dopplerLevel = 0f;
                _audioSource.rolloffMode = AudioRolloffMode.Custom;
            }

            m_update = () =>
            {
                if (_audioSource.isPlaying || _audioSource.time != 0f) { return; }
                Stop();
            };

            f_init();
        }

        private void OnDestroy()
        {
            m_provider.RemoveSound(this);
            Updater.Frame_Event -= m_update;
        }

        ////////////////
        //Public Funcs//
        ////////////////

        internal void Play(SoundProvider provider, object sender, AudioClip clip, SPreset set)
        {
            _audioSource.clip = clip;

            m_provider = provider;
            Sender = sender;

            f_play(set);
        }

        internal void Play3D(SoundProvider provider, AudioClip clip, SPreset set, Vector3 pos)
        {
            _audioSource.clip = clip;
            _audioSource.minDistance = set.MinDist;
            _audioSource.maxDistance = set.MaxDist;
            _audioSource.spatialBlend = 1f;

            m_provider = provider;

            transform.position = pos;

            f_play(set);
        }

        internal void Play3D(SoundProvider provider, AudioClip clip, SPreset set, Transform sender)
        {
            _audioSource.clip = clip;
            _audioSource.minDistance = set.MinDist;
            _audioSource.maxDistance = set.MaxDist;
            _audioSource.spatialBlend = 1f;

            m_provider = provider;
            Sender = sender;

            transform.SetParent(sender, Vector3.zero);

            f_play(set);
        }

        internal void Restart(SPreset set)
        {
            StopAllCoroutines();
            _audioSource.Stop();
            f_play(set);
        }

        internal override void Stop()
        {
            StopAllCoroutines();
            _audioSource.Stop();
            m_provider.ReleaseSound(this);
        }

        internal void StopFading(float time)
        {
            StopAllCoroutines();
            StartCoroutine(FadeAndStop(time));
        }

        internal void Mute(bool value)
        {
            _audioSource.mute = value;
        }

        internal void UpdVolume()
        {
            _audioSource.volume = m_volume * m_provider.Volume;
        }

        internal void UpdPitch()
        {
            _audioSource.pitch = m_pitch * m_provider.Pitch;
        }

        internal void Pause()
        {
            _audioSource.Pause();
        }

        internal void UnPause()
        {
            _audioSource.UnPause();
        }

        ///////////////
        //Inner Funcs//
        ///////////////

        private void f_init()
        {
            Updater.Frame_Event += m_update;
        }

        #region IPoolable
        void Poolable.Reinit()
        {
            gameObject.SetActive(true);
            f_init();
        }

        void Poolable.CleanUp()
        {
            if (Sender != null && Sender is Transform) { transform.Free(); }
            gameObject.SetActive(false);
            _audioSource.clip = null;
            _audioSource.spatialBlend = 0f;
            Sender = null;
            Updater.Frame_Event -= m_update;
        }
        #endregion

        //////////////
        //Inner fncs//
        //////////////

        private void f_play(SPreset set)
        {
            m_volume = set.Volume;
            m_pitch = set.Pitch;
            _audioSource.loop = Sender == null ? false : set.Looped;
            _audioSource.mute = m_provider.Muted;
            UpdVolume();
            UpdPitch();
            _audioSource.Play();
        }

        ////////////
        //Routines//
        ////////////

        IEnumerator FadeAndStop(float time)
        {
            if (time < 0f)
                throw new ArgumentOutOfRangeException(nameof(time), "Time cannot be negative.");

            float startVal = m_volume;
            float curTime = 0f;
            float ratio = 0f;

            do
            {
                yield return null;

                if (m_provider.Paused) { continue; }

                curTime += Time.unscaledDeltaTime * m_provider.Pitch.Abs();
                ratio = curTime / time;
                m_volume = Mathf.Lerp(startVal, 0f, ratio);
                UpdVolume();

            } while (ratio < 1f);

            _audioSource.Stop();
            m_provider.ReleaseSound(this);
        }
    }
}
