﻿using System;
using System.Text;

namespace Hargrim.SaveLoad.SaveProviderStuff
{
    public class BaseKeyGenerator : KeyGenerator
    {
        private readonly char SEP = '.';
        private readonly string EXT = "key";

        private StringBuilder m_builder = new StringBuilder();

        public string Generate(Type objectType, string fieldName, string objectID)
        {
            m_builder.Clear();

            m_builder.Append(objectType.FullName)
                     .Append(SEP)
                     .Append(fieldName)
                     .Append(SEP);

            if (objectID.HasUsefulData())
                m_builder.Append(objectID);

            m_builder.Append(EXT);

            return m_builder.ToString();
        }
    }
}
