﻿using Hargrim.Collections;
using Hargrim.MathExt;
using Hargrim.RNGenerators;
using System;
using System.Collections.Unsafe;
using System.Linq;

namespace Hargrim
{
    /// <summary>
    /// Class for generating random data.
    /// </summary>
    public static class Rnd
    {
        private const float MIN_STEP = 0.0000001f;

        private static RNG m_rng;

        static Rnd()
        {
            m_rng = new DotNetRNG();
        }

        public static void OverrideRandomizer(RNG randomizer)
        {
            m_rng = randomizer;
        }

        /// <summary>
        /// Returns true with chance from 0f to 1f.
        /// </summary>
        public static bool Chance(float chance)
        {
            return chance > m_rng.NextFloat(0f, 1f - MIN_STEP);
        }

        /// <summary>
        /// Returns true with chance represented by Percent from 0 to 100.
        /// </summary>
        public static bool Chance(Percent chance)
        {
            return Chance(chance.ToRatio());
        }

        /// <summary>
        /// Returns index of chances array or -1 for none of the elements. Sum of the elements must not be more than 1f. Each element cannot be less than 0f.
        /// </summary>
        public static int Chance(float[] chances)
        {
            float rnd = m_rng.NextFloat(0f, 1f - MIN_STEP);
            float sum = 0f;

            for (int i = 0; i < chances.Length; i++)
            {
                if (chances[i] + sum > rnd)
                    return i;

                sum += chances[i];
            }

            return -1;
        }

        /// <summary>
        /// Returns index of chances array or -1 for none of the elements. Sum of the elements must not be more than 1f. Each element cannot be less than 0f.
        /// </summary>
        public static unsafe int Chance(float* chances, int length)
        {
            if (chances == null)
                throw new ArgumentNullException(nameof(chances), "Pointer cannot be null.");

            float rnd = m_rng.NextFloat(0f, 1f - MIN_STEP);
            float sum = 0f;

            for (int i = 0; i < length; i++)
            {
                if (chances[i] + sum > rnd)
                    return i;

                sum += chances[i];
            }

            return -1;
        }

        /// <summary>
        /// Returns index of chances array contains chance ratios. Each element cannot be less than 0f.
        /// </summary>
        public static int RatioChance(float[] chances)
        {
            float rnd = m_rng.NextFloat(0f, chances.Sum() - MIN_STEP);
            float sum = 0f;

            for (int i = 0; i < chances.Length; i++)
            {
                if (chances[i] + sum > rnd)
                    return i;

                sum += chances[i];
            }

            return -1;
        }

        /// <summary>
        /// Returns index of chances array contains chance ratios. Each element cannot be less than 0f.
        /// </summary>
        public static unsafe int RatioChance(float* chances, int length)
        {
            float rnd = m_rng.NextFloat(0f, StackArray.Sum(chances, length) - MIN_STEP);
            float sum = 0f;

            for (int i = 0; i < length; i++)
            {
                if (chances[i] + sum > rnd)
                    return i;

                sum += chances[i];
            }

            return -1;
        }

        /// <summary>
        /// Returns index of chances array contains chance ratios. Each element cannot be less than 0.
        /// </summary>
        public static int RatioChance(int[] chances)
        {
            int rnd = m_rng.Next(chances.Sum());
            int sum = 0;

            for (int i = 0; i < chances.Length; i++)
            {
                if (chances[i] + sum > rnd)
                    return i;

                sum += chances[i];
            }

            return -1;
        }

        /// <summary>
        /// Returns index of chances array contains chance ratios. Each element cannot be less than 0.
        /// </summary>
        public static unsafe int RatioChance(int* chances, int length)
        {
            int rnd = m_rng.Next(StackArray.Sum(chances, length));
            int sum = 0;

            for (int i = 0; i < length; i++)
            {
                if (chances[i] + sum > rnd)
                    return i;

                sum += chances[i];
            }

            return -1;
        }

        /// <summary>
        /// Returns a random integer number between min [inclusive] and max [exclusive].
        /// </summary>
        public static int Random(int min, int max)
        {
            return m_rng.Next(min, max);
        }

        /// <summary>
        /// Returns a random integer number between zero [inclusive] and max [exclusive].
        /// </summary>
        public static int Random(int max)
        {
            return m_rng.Next(max);
        }

        /// <summary>
        /// Returns a random float number between min [inclusive] and max [inclusive].
        /// </summary>
        public static float Random(float min, float max)
        {
            return m_rng.NextFloat(min, max);
        }

        /// <summary>
        /// Returns a random float number between -range [inclusive] and range [inclusive].
        /// </summary>
        public static float Range(float range)
        {
            return m_rng.NextFloat(-range, range);
        }

        /// <summary>
        /// Returns a random flag contains in the specified mask.
        /// </summary>
        /// <param name="length">How many flags of 32bit mask should be considered.</param>
        public static int Random(BitMask mask, int length)
        {
            int rn = Random(mask.GetCount(length));

            for (int i = 0; i < length; i++)
            {
                if (mask.ContainsFlag(i))
                {
                    if (rn-- == 0)
                        return i;
                }
            }

            throw new InvalidOperationException("Mask is empty.");
        }

        /// <summary>
        /// Returns a random flag contains in the specified mask.
        /// </summary>
        public static int Random(BitArrayMask mask)
        {
            int rn = Random(mask.GetCount());

            for (int i = 0; i < mask.Length; i++)
            {
                if (mask.Get(i))
                {
                    if (rn-- == 0)
                        return i;
                }
            }

            throw new InvalidOperationException("Mask is empty.");
        }

        #region randoms by condition
        //TODO: need check of impossible condition

        /// <summary>
        /// Returns a random integer number between min [inclusive] and max [exclusive] and which is not equal to exclusiveValue.
        /// </summary>
        public static int Random(int min, int max, int exclusiveValue)
        {
            int value;
            do { value = m_rng.Next(min, max); } while (value == exclusiveValue);
            return value;
        }

        /// <summary>
        /// Returns a random integer number between min [inclusive] and max [exclusive] and which is satisfies the specified condition.
        /// </summary>
        public static int Random(int min, int max, Func<int, bool> condition)
        {
            int value;
            do { value = m_rng.Next(min, max); } while (!condition(value));
            return value;
        }

        /// <summary>
        /// Returns a random float number between min [inclusive] and max [inclusive] and which is satisfies the specified condition.
        /// </summary>
        public static float Random(float min, float max, Func<float, bool> condition)
        {
            float value;
            do { value = m_rng.NextFloat(min, max); } while (!condition(value));
            return value;
        }

        /// <summary>
        /// Returns a random flag contains in the specified mask and which is satisfies the specified condition.
        /// </summary>
        public static int Random(BitMask mask, int length, Func<int, bool> condition)
        {
            int value;
            do { value = Random(mask, length); } while (!condition(value));
            return value;
        }

        /// <summary>
        /// Returns a random flag contains in the specified mask and which is satisfies the specified condition.
        /// </summary>
        public static int Random(BitArrayMask mask, Func<int, bool> condition)
        {
            int value;
            do { value = Random(mask); } while (!condition(value));
            return value;
        }
        #endregion

        /// <summary>
        /// Returns a random float number between min [inclusive] and max [inclusive] with chance offset to min values.
        /// </summary>
        public static float Descending(float min, float max)
        {
            if (min > max)
                throw new ArgumentOutOfRangeException(nameof(min), string.Format("{0} cannot be more than {1}.", nameof(min), nameof(max)));

            float range = max - min;
            float rnd = m_rng.NextFloat(0f, 1f);
            return rnd * rnd * range + min;
        }

        /// <summary>
        /// Returns a random float number between min [inclusive] and max [inclusive] with chance offset to max values.
        /// </summary>
        public static float Ascending(float min, float max)
        {
            if (min > max)
                throw new ArgumentOutOfRangeException(nameof(min), string.Format("{0} cannot be more than {1}.", nameof(min), nameof(max)));

            float range = max - min;
            float rnd = m_rng.NextFloat(0f, 1f);
            return rnd.Sqrt() * range + min;
        }

        /// <summary>
        /// Returns a random float number between min [inclusive] and max [inclusive] with chance offset to min values if curvature &gt; 1f or to max values if curvature &lt; 1f.
        /// </summary>
        /// <param name="curvature">Power of the offset dependency (cannot be negative). If the value is 1f the function has no chance offset because of linear dependency.</param>
        public static float Side(float min, float max, float curvature)
        {
            if (min > max)
                throw new ArgumentOutOfRangeException(nameof(min), string.Format("{0} cannot be more than {1}.", nameof(min), nameof(max)));

            if (curvature < 0f)
                throw new ArgumentOutOfRangeException(nameof(curvature), nameof(curvature) + " cannot be negative.");

            float range = max - min;
            float rnd = m_rng.NextFloat(0f, 1f);
            return rnd.Pow(curvature) * range + min;
        }

        /// <summary>
        /// Returns a random float number between min [inclusive] and max [inclusive] with chance offset to min and max values if curvature &gt; 1f or to average values if curvature &lt; 1f.
        /// </summary>
        /// <param name="curvature">Power of the offset dependency (cannot be negative). If the value is 1f the function has no chance offset because of linear dependency.</param>
        public static float Symmetric(float min, float max, float curvature)
        {
            float average = (max + min) * 0.5f;

            if (m_rng.Next(0, 2) == 0)
                return Side(min, average, curvature);
            else
                return Side(average, max, 1f / curvature);
        }

        /// <summary>
        /// Returns a random even integer number between min [inclusive] and max [exclusive].
        /// </summary>
        public static int RandomEven(int min, int max)
        {
            return m_rng.Next(min, max) & -2;
        }

        /// <summary>
        /// Returns a random odd integer number between min [inclusive] and max [exclusive].
        /// </summary>
        public static int RandomOdd(int min, int max)
        {
            return m_rng.Next(min, max - 1) | 1; //TODO: need check for (max - 1).
        }

        /// <summary>
        /// Fills a byte array with random values.
        /// </summary>
        public static void RandomByteArray(byte[] buffer)
        {
            m_rng.NextBytes(buffer);
        }

        /// <summary>
        /// Fills a byte array with random values.
        /// </summary>
        public static unsafe void RandomByteArray(byte* arrayPtr, int length)
        {
            m_rng.NextBytes(arrayPtr, length);
        }
    }
}
