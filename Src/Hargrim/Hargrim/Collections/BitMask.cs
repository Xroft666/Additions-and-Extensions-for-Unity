using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hargrim.Collections
{
    /// <summary>
    /// Integer bit mask. Can contain 32 flags.
    /// Supports casting to int and back.
    /// </summary>
    [Serializable]
    public struct BitMask : IEquatable<BitMask>, IEnumerable<bool>
    {
        public const int SIZE = 32;

        [SerializeField, HideInInspector]
        private int m_field;

        internal static string SerFieldName
        {
            get { return nameof(m_field); }
        }

        public bool Empty
        {
            get { return m_field == 0; }
        }

        public bool NotEmpty
        {
            get { return m_field != 0; }
        }

        public bool Full
        {
            get { return m_field == -1; }
        }

        public bool this[int index]
        {
            get { return ContainsFlag(index); }
            set
            {
                if (value) { AddFlag(index); }
                else { RemoveFlag(index); }
            }
        }

        public BitMask(bool defaultValue)
        {
            m_field = defaultValue ? -1 : 0;
        }

        public BitMask(int flag0)
        {
            m_field = 1 << flag0;
        }

        public BitMask(int flag0, int flag1)
        {
            m_field = 1 << flag0;
            m_field |= 1 << flag1;
        }

        public BitMask(int flag0, int flag1, int flag2)
        {
            m_field = 1 << flag0;
            m_field |= 1 << flag1;
            m_field |= 1 << flag2;
        }

        public BitMask(int flag0, int flag1, int flag2, int flag3)
        {
            m_field = 1 << flag0;
            m_field |= 1 << flag1;
            m_field |= 1 << flag2;
            m_field |= 1 << flag3;
        }

        public BitMask(params int[] flags)
        {
            m_field = 0;
            for (int i = 0; i < flags.Length; i++)
            {
                m_field |= 1 << flags[i];
            }
        }

        public void AddFlag(int index)
        {
            m_field |= 1 << index;
        }

        public void RemoveFlag(int index)
        {
            m_field &= ~(1 << index);
        }

        public void SwitchFlag(int index)
        {
            m_field ^= 1 << index;
        }

        public bool ContainsFlag(int index)
        {
            return (m_field & 1 << index) != 0;
        }

        public void SetFlag(int index, bool value)
        {
            if (value) { AddFlag(index); }
            else { RemoveFlag(index); }
        }

        public void AddFlags(BitMask flags)
        {
            m_field |= flags.m_field;
        }

        public void RemoveFlags(BitMask flags)
        {
            m_field ^= m_field & flags.m_field;
        }

        public void SetFlags(BitMask flags, bool value)
        {
            if (value) { AddFlags(flags); }
            else { RemoveFlags(flags); }
        }

        public void AddAll(int length = SIZE)
        {
            if (length > SIZE)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (length == SIZE)
            {
                m_field = -1;
            }
            else
            {
                for (int i = 0; i < length; i++)
                {
                    AddFlag(i);
                }
            }
        }

        public void Clear()
        {
            m_field = 0;
        }

        public void SetAll(bool value, int length = SIZE)
        {
            if (value) { AddAll(length); }
            else { Clear(); }
        }

        public void InvertAll()
        {
            m_field = ~m_field;
        }

        public bool AllOf(BitMask flags)
        {
            for (int i = 0; i < SIZE; i++)
            {
                if (flags.ContainsFlag(i) && !ContainsFlag(i))
                    return false;
            }

            return true;
        }

        public bool AnyOf(BitMask flags)
        {
            for (int i = 0; i < SIZE; i++)
            {
                if (flags.ContainsFlag(i) && ContainsFlag(i))
                    return true;
            }

            return false;
        }

        public bool AllFor(int length)
        {
            if (length > SIZE)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (length == SIZE)
                return Full;

            for (int i = 0; i < length; i++)
            {
                if (!ContainsFlag(i))
                    return false;
            }
            return true;
        }

        public bool AnyFor(int length)
        {
            if (length > SIZE)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (length == SIZE)
                return NotEmpty;

            for (int i = 0; i < length; i++)
            {
                if (ContainsFlag(i))
                    return true;
            }
            return false;
        }

        public int GetCount(int length = SIZE)
        {
            if (length > SIZE)
                throw new ArgumentOutOfRangeException(nameof(length));

            if (length == SIZE)
                return GetUnitsCount(m_field);

            int count = 0;
            for (int i = 0; i < length; i++)
            {
                if (ContainsFlag(i))
                    count++;
            }
            return count;
        }

        public BitArrayMask ToBitArray(int length = SIZE)
        {
            return new BitArrayMask(this, length);
        }

        public LayerMask ToLayerMask()
        {
            return m_field;
        }

        internal static int GetUnitsCount(int mask)
        {
            mask -= (mask >> 1) & 0x55555555;
            mask = ((mask >> 2) & 0x33333333) + (mask & 0x33333333);
            mask = ((((mask >> 4) + mask) & 0x0F0F0F0F) * 0x01010101) >> 24;
            return mask;
        }

        #region regular stuff
        public override int GetHashCode()
        {
            return m_field;
        }

        public bool Equals(BitMask other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            return obj is BitMask && this == (BitMask)obj;
        }

        public override string ToString()
        {
            return m_field.ToString();
        }

        public IEnumerator<bool> GetEnumerator()
        {
            for (int i = 0; i < SIZE; i++)
            {
                yield return ContainsFlag(i);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        //--//

        public static implicit operator BitMask(int value)
        {
            return new BitMask { m_field = value };
        }

        public static implicit operator int(BitMask value)
        {
            return value.m_field;
        }

        public static bool operator ==(BitMask a, BitMask b)
        {
            return a.m_field == b.m_field;
        }

        public static bool operator !=(BitMask a, BitMask b)
        {
            return a.m_field != b.m_field;
        }

        public static BitMask operator &(BitMask a, BitMask b)
        {
            a.m_field &= b.m_field;
            return a;
        }

        public static BitMask operator |(BitMask a, BitMask b)
        {
            a.m_field |= b.m_field;
            return a;
        }

        public static BitMask operator ^(BitMask a, BitMask b)
        {
            a.m_field ^= b.m_field;
            return a;
        }

        public static BitMask operator ~(BitMask a)
        {
            a.m_field = ~a.m_field;
            return a;
        }
    }
}
