﻿using System;
using static Hargrim.Collections.TrackerNodes;

namespace Hargrim.Collections
{
    public class Tracker : Refreshable
    {
        private readonly Node m_dNode;
        private Node m_rNode;

        public Tracker()
        {
            m_dNode = m_rNode = new EmptyNode();
        }

        public void Refresh()
        {
            m_rNode.Check();
        }

        public void Force()
        {
            m_rNode.Force();
        }

        public void AddValueNode<T>(Func<T> getFunc, Action<T> setFunc) where T : struct, IEquatable<T>
        {
            m_rNode = new ValueNode<T>(getFunc, setFunc, m_rNode);
        }

        public void AddValueNode<T1, T2>(Func<T1> getFunc1, Func<T2> getFunc2, Action<T1, T2> setFunc) where T1 : struct, IEquatable<T1> where T2 : struct, IEquatable<T2>
        {
            m_rNode = new ValueNode<T1, T2>(getFunc1, getFunc2, setFunc, m_rNode);
        }

        public void AddObjectNode<T>(Func<T> getFunc, Action<T> setFunc) where T : class
        {
            m_rNode = new ObjectNode<T>(getFunc, setFunc, m_rNode);
        }

        public void AddObjectNode<TObject, TState>(Func<TObject> getFunc, Func<TState> getStateFunc, Action<TObject> setFunc) where TObject : class where TState : struct, IEquatable<TState>
        {
            m_rNode = new ObjectNode<TObject, TState>(getFunc, getStateFunc, setFunc, m_rNode);
        }

        public void AddNodeRelative<T>(Action<T> setFunc)
        {
            m_rNode = new RelativeNode<T>(setFunc, m_rNode);
        }

        public void AddNodeRelative<T1, T2>(Action<T1> setFunc)
        {
            m_rNode = new RelativeNode<T1, T2>(setFunc, m_rNode);
        }

        public void AddNodeRelative<T1, T2>(Action<T1, T2> setFunc)
        {
            m_rNode = new RelativeNode<T1, T2>(setFunc, m_rNode);
        }

        public void Clear()
        {
            m_rNode = m_dNode;
        }
    }
}
