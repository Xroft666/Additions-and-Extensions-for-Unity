﻿using System;

namespace Hargrim.Collections
{
    internal static class TrackerNodes
    {
        public interface Node
        {
            bool Changed { get; }
            void Check();
            void Force();
        }

        public interface Node<T> : Node
        {
            T Value { get; }
        }

        public interface Node<T1, T2> : Node
        {
            T1 Value1 { get; }
            T2 Value2 { get; }
        }

        // -- //

        public class EmptyNode : Node
        {
            bool Node.Changed
            {
                get { return false; }
            }

            void Node.Check() { }
            void Node.Force() { }
        }

        // -- //

        public abstract class NodeKeeper : Node
        {
            protected Node m_prevNode;

            public NodeKeeper(Node prevNode)
            {
                m_prevNode = prevNode;
            }

            public abstract bool Changed { get; }

            public void Check()
            {
                m_prevNode.Check();
                InnerCheck();
            }

            public void Force()
            {
                m_prevNode.Force();
                InnerForce();
            }

            protected abstract void InnerCheck();
            protected abstract void InnerForce();
        }

        // -- //

        public class ValueNode<T> : NodeKeeper, Node<T> where T : struct, IEquatable<T>
        {
            private Func<T> m_getFunc;
            private Action<T> m_setFunc;

            private T m_cache;
            private bool m_changed;

            public override bool Changed
            {
                get { return m_changed; }
            }

            T Node<T>.Value
            {
                get { return m_cache; }
            }

            public ValueNode(Func<T> getFunc, Action<T> setFunc, Node prevNode) : base(prevNode)
            {
                m_getFunc = getFunc;
                m_setFunc = setFunc;
            }

            protected override void InnerCheck()
            {
                T tmp = m_getFunc();

                if (m_changed = !m_cache.Equals(tmp))
                {
                    m_cache = tmp;
                    m_setFunc(tmp);
                }
            }

            protected override void InnerForce()
            {
                m_cache = m_getFunc();
                m_setFunc(m_cache);
            }
        }

        // -- //

        public class ValueNode<T1, T2> : NodeKeeper, Node<T1, T2> where T1 : struct, IEquatable<T1> where T2 : struct, IEquatable<T2>
        {
            private Func<T1> m_getFunc1;
            private Func<T2> m_getFunc2;
            private Action<T1, T2> m_setFunc;

            private T1 m_cache1;
            private T2 m_cache2;
            private bool m_changed;

            public override bool Changed
            {
                get { return m_changed; }
            }

            public T1 Value1
            {
                get { return m_cache1; }
            }

            public T2 Value2
            {
                get { return m_cache2; }
            }

            public ValueNode(Func<T1> getFunc1, Func<T2> getFunc2, Action<T1, T2> setFunc, Node prevNode) : base(prevNode)
            {
                m_getFunc1 = getFunc1;
                m_getFunc2 = getFunc2;
                m_setFunc = setFunc;
            }

            protected override void InnerCheck()
            {
                T1 tmp1 = m_getFunc1();
                T2 tmp2 = m_getFunc2();

                if (m_changed = !m_cache1.Equals(tmp1) || !m_cache2.Equals(tmp2))
                {
                    m_cache1 = tmp1;
                    m_cache2 = tmp2;
                    m_setFunc(tmp1, tmp2);
                }
            }

            protected override void InnerForce()
            {
                m_cache1 = m_getFunc1();
                m_cache2 = m_getFunc2();
                m_setFunc(m_cache1, m_cache2);
            }
        }

        // -- //

        public class ObjectNode<TObject> : NodeKeeper, Node<TObject> where TObject : class
        {
            private Func<TObject> m_getFunc;
            private Action<TObject> m_setFunc;

            private TObject m_objectCached;

            private bool m_changed;

            public override bool Changed
            {
                get { return m_changed; }
            }

            public TObject Value
            {
                get { return m_objectCached; }
            }

            public ObjectNode(Func<TObject> getFunc, Action<TObject> setFunc, Node prevNode) : base(prevNode)
            {
                m_getFunc = getFunc;
                m_setFunc = setFunc;
            }

            protected override void InnerCheck()
            {
                TObject tmp = m_getFunc();

                if (m_changed = m_objectCached != tmp)
                {
                    m_objectCached = tmp;
                    m_setFunc(tmp);
                }
            }

            protected override void InnerForce()
            {
                m_objectCached = m_getFunc();
                m_setFunc(m_objectCached);
            }
        }

        // -- //

        public class ObjectNode<TObject, TState> : NodeKeeper, Node<TObject, TState> where TObject : class where TState : struct, IEquatable<TState>
        {
            private Func<TObject> m_getFunc;
            private Func<TState> m_getStateFunc;
            private Action<TObject> m_setFunc;

            private TObject m_objectCached;
            private TState m_stateCached;

            private bool m_changed;

            public override bool Changed
            {
                get { return m_changed; }
            }

            public TObject Value1
            {
                get { return m_objectCached; }
            }

            public TState Value2
            {
                get { return m_stateCached; }
            }

            public ObjectNode(Func<TObject> getFunc, Func<TState> getStateFunc, Action<TObject> setFunc, Node prevNode) : base(prevNode)
            {
                m_getFunc = getFunc;
                m_setFunc = setFunc;
                m_getStateFunc = getStateFunc;
            }

            protected override void InnerCheck()
            {
                TObject tmp = m_getFunc();
                TState tmpState = m_getStateFunc();

                if (m_changed = m_objectCached != tmp || !m_stateCached.Equals(tmpState))
                {
                    m_objectCached = tmp;
                    m_stateCached = tmpState;
                    m_setFunc(tmp);
                }
            }

            protected override void InnerForce()
            {
                m_objectCached = m_getFunc();
                m_stateCached = m_getStateFunc();
                m_setFunc(m_objectCached);
            }
        }

        // -- //

        public class RelativeNode<T> : NodeKeeper
        {
            private Action<T> m_setFunc;
            private Node<T> m_relativeTo;

            public override bool Changed
            {
                get { return m_relativeTo.Changed; }
            }

            public RelativeNode(Action<T> setFunc, Node prevNode) : base(prevNode)
            {
                m_setFunc = setFunc;
                m_relativeTo = (Node<T>)prevNode;
            }

            protected override void InnerCheck()
            {
                if (m_relativeTo.Changed)
                {
                    m_setFunc(m_relativeTo.Value);
                }
            }

            protected override void InnerForce()
            {
                m_setFunc(m_relativeTo.Value);
            }
        }

        // -- //

        public class RelativeNode<T1, T2> : NodeKeeper
        {
            private Action<T1> m_setFunc1;
            private Action<T1, T2> m_setFunc2;
            private Node<T1, T2> m_relativeTo;

            public override bool Changed
            {
                get { return m_relativeTo.Changed; }
            }

            private RelativeNode(Node prevNode) : base(prevNode)
            {
                m_relativeTo = (Node<T1, T2>)prevNode;
            }

            public RelativeNode(Action<T1> setFunc, Node prevNode) : this(prevNode)
            {
                m_setFunc1 = setFunc;
            }

            public RelativeNode(Action<T1, T2> setFunc, Node prevNode) : this(prevNode)
            {
                m_setFunc2 = setFunc;
            }

            protected override void InnerCheck()
            {
                if (m_relativeTo.Changed)
                {
                    m_setFunc1?.Invoke(m_relativeTo.Value1);
                    m_setFunc2?.Invoke(m_relativeTo.Value1, m_relativeTo.Value2);
                }
            }

            protected override void InnerForce()
            {
                m_setFunc1?.Invoke(m_relativeTo.Value1);
                m_setFunc2?.Invoke(m_relativeTo.Value1, m_relativeTo.Value2);
            }
        }
    }
}
