﻿using System;
using UnityEngine;

namespace Hargrim.Collections
{
    [AttributeUsage(AttributeTargets.Field)]
    public class BitMaskEnumFlagsAttribute : PropertyAttribute
    {
        public readonly Type EnumType;

        public BitMaskEnumFlagsAttribute(Type enumType)
        {
            EnumType = enumType;
        }
    }
}
