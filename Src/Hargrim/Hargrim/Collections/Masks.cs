﻿using System;
using System.Collections.Generic;

namespace Hargrim.Collections
{
    public static class Masks
    {
        public static BitMask CreateMask<TEnum>(TEnum flag0) where TEnum : Enum
        {
            return new BitMask(flag0.ToInteger());
        }

        public static BitMask CreateMask<TEnum>(TEnum flag0, TEnum flag1) where TEnum : Enum
        {
            return new BitMask(flag0.ToInteger(), flag1.ToInteger());
        }

        public static BitMask CreateMask<TEnum>(TEnum flag0, TEnum flag1, TEnum flag2) where TEnum : Enum
        {
            return new BitMask(flag0.ToInteger(), flag1.ToInteger(), flag2.ToInteger());
        }

        public static BitMask CreateMask<TEnum>(TEnum flag0, TEnum flag1, TEnum flag2, TEnum flag3) where TEnum : Enum
        {
            return new BitMask(flag0.ToInteger(), flag1.ToInteger(), flag2.ToInteger(), flag3.ToInteger());
        }

        public static BitMask CreateMask<TEnum>(params TEnum[] flags) where TEnum : Enum
        {
            int mask = 0;

            for (int i = 0; i < flags.Length; i++)
            {
                mask |= 1 << flags[i].ToInteger();
            }

            return mask;
        }

        // -- //

        public static BitArrayMask CreateArray<TEnum>(TEnum flag0) where TEnum : Enum
        {
            return new BitArrayMask(EnumExt<TEnum>.Count, flag0.ToInteger());
        }

        public static BitArrayMask CreateArray<TEnum>(TEnum flag0, TEnum flag1) where TEnum : Enum
        {
            return new BitArrayMask(EnumExt<TEnum>.Count, flag0.ToInteger(), flag1.ToInteger());
        }

        public static BitArrayMask CreateArray<TEnum>(TEnum flag0, TEnum flag1, TEnum flag2) where TEnum : Enum
        {
            return new BitArrayMask(EnumExt<TEnum>.Count, flag0.ToInteger(), flag1.ToInteger(), flag2.ToInteger());
        }

        public static BitArrayMask CreateArray<TEnum>(TEnum flag0, TEnum flag1, TEnum flag2, TEnum flag3) where TEnum : Enum
        {
            return new BitArrayMask(EnumExt<TEnum>.Count, flag0.ToInteger(), flag1.ToInteger(), flag2.ToInteger(), flag3.ToInteger());
        }

        public static BitArrayMask CreateArray<TEnum>(params TEnum[] flags) where TEnum : Enum
        {
            return new BitArrayMask(EnumExt<TEnum>.Count, flags.GetConverted(itm => itm.ToInteger()));
        }
    }
}
