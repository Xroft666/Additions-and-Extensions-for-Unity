﻿using System;

namespace Hargrim.Collections
{
    public static class BitMaskExtensions
    {
        public static void Add<TEnum>(this ref BitMask mask, TEnum flag) where TEnum : Enum
        {
            mask.AddFlag(flag.ToInteger());
        }

        public static void Remove<TEnum>(this ref BitMask mask, TEnum flag) where TEnum : Enum
        {
            mask.RemoveFlag(flag.ToInteger());
        }

        public static void Switch<TEnum>(this ref BitMask mask, TEnum flag) where TEnum : Enum
        {
            mask.SwitchFlag(flag.ToInteger());
        }

        public static bool Contains<TEnum>(this in BitMask mask, TEnum flag) where TEnum : Enum
        {
            return mask.ContainsFlag(flag.ToInteger());
        }

        public static void Set<TEnum>(this ref BitMask mask, TEnum flag, bool value) where TEnum : Enum
        {
            mask.SetFlag(flag.ToInteger(), value);
        }

        public static void AddAll<TEnum>(this ref BitMask mask) where TEnum : Enum
        {
            mask.AddAll(EnumExt<TEnum>.Count);
        }

        public static void SetAll<TEnum>(this ref BitMask mask, bool value) where TEnum : Enum
        {
            mask.SetAll(value, EnumExt<TEnum>.Count);
        }

        public static bool All<TEnum>(this in BitMask mask) where TEnum : Enum
        {
            return mask.AllFor(EnumExt<TEnum>.Count);
        }

        public static bool Any<TEnum>(this in BitMask mask) where TEnum : Enum
        {
            return mask.AnyFor(EnumExt<TEnum>.Count);
        }

        public static int GetCount<TEnum>(this in BitMask mask) where TEnum : Enum
        {
            return mask.GetCount(EnumExt<TEnum>.Count);
        }

        // -- //

        public static void Set<TEnum>(this BitArrayMask mask, TEnum flag, bool value) where TEnum : Enum
        {
            mask.Set(flag.ToInteger(), value);
        }

        public static bool Get<TEnum>(this BitArrayMask mask, TEnum flag) where TEnum : Enum
        {
            return mask.Get(flag.ToInteger());
        }

        public static void Add<TEnum>(this BitArrayMask mask, TEnum flag) where TEnum : Enum
        {
            mask.Set(flag.ToInteger(), true);
        }

        public static void Remove<TEnum>(this BitArrayMask mask, TEnum flag) where TEnum : Enum
        {
            mask.Set(flag.ToInteger(), false);
        }

        public static void Switch<TEnum>(this BitArrayMask mask, TEnum flag) where TEnum : Enum
        {
            int index = flag.ToInteger();
            mask.Set(index, !mask.Get(index));
        }
    }
}
