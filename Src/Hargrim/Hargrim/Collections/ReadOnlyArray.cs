﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

#pragma warning disable CS0659, CS0661, IDE0016
namespace Hargrim.Collections
{
    public struct ReadOnlyArray<T> : IEnumerable<T>, IEquatable<ReadOnlyArray<T>>
    {
        private IList<T> m_list;

        public T this[int index]
        {
            get { return m_list[index]; }
        }

        public int Count
        {
            get { return m_list.Count; }
        }

        public ReadOnlyArray(IList<T> collection)
        {
            if (collection == null)
                throw new ArgumentNullException(nameof(collection));

            m_list = collection;
        }

        public bool Contains(T item)
        {
            return m_list.Contains(item);
        }

        public int IndexOf(T item)
        {
            return m_list.IndexOf(item);
        }

        public T GetLast()
        {
            return m_list[m_list.Count - 1];
        }

        public ReadOnlyCollection<T> ToCollection()
        {
            return new ReadOnlyCollection<T>(m_list);
        }

        public T[] ToArray()
        {
            return m_list.ToArray();
        }

        // -- //

        public override bool Equals(object obj)
        {
            return obj is ReadOnlyArray<T> && this == (ReadOnlyArray<T>)obj;
        }

        public bool Equals(ReadOnlyArray<T> other)
        {
            return other == this;
        }

        public override int GetHashCode()
        {
            return m_list.GetHashCode();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return m_list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return m_list.GetEnumerator();
        }

        // -- //

        public static bool operator ==(ReadOnlyArray<T> a, ReadOnlyArray<T> b)
        {
            return a.m_list == b.m_list;
        }

        public static bool operator !=(ReadOnlyArray<T> a, ReadOnlyArray<T> b)
        {
            return a.m_list != b.m_list;
        }
    }
}
