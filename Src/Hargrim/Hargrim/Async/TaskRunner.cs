﻿using System.Collections;

namespace Hargrim.Async
{
    /// <summary>
    /// Coroutine runner. Allows to run coroutines from non-behaviuor objects. Can run only one coroutine at the time.
    /// </summary>
    public sealed class TaskRunner : RoutineExecutorOwner
    {
        private RoutineExecutor m_executor;

        /// <summary>
        /// Returns true is the runner contains any active task.
        /// </summary>
        public bool IsRunning
        {
            get { return m_executor != null; }
        }

        /// <summary>
        /// Adds task to the queue. Returns reference to itself.
        /// </summary>
        public TaskRunner Add(IEnumerator routine)
        {
            f_getExecutor().Add(routine);
            return this;
        }

        /// <summary>
        /// Starts the task queue.
        /// </summary>
        public void Start()
        {
            if (m_executor) { m_executor.StartRunning(); }
        }

        /// <summary>
        /// Skips the current task and runs the next one.
        /// </summary>
        public void SkipCurrent()
        {
            if (m_executor) { m_executor.SkipCurrent(); }
        }

        /// <summary>
        /// Pauses running.
        /// </summary>
        public void Pause()
        {
            if (m_executor) { m_executor.Pause(); }
        }

        /// <summary>
        /// Resumes running.
        /// </summary>
        public void Resume()
        {
            if (m_executor) { m_executor.Resume(); }
        }

        /// <summary>
        /// Stops running and clears the queue.
        /// </summary>
        public void Stop()
        {
            if (m_executor)
            {
                m_executor.Stop();
            }
        }

        // - - //        

        private RoutineExecutor f_getExecutor()
        {
            if (m_executor == null)
            {
                m_executor = AsyncStuffPool.GetExecutor();
                m_executor.SetUp(this);
            }

            return m_executor;
        }

        void RoutineExecutorOwner.RemoveExecutor(RoutineExecutor executor)
        {
            m_executor = null;
        }
    }
}
