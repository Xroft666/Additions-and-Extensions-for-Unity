﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Hargrim.Async
{
    /// <summary>
    /// Coroutine runner. Allows to run coroutines from non-behaviuor objects.
    /// </summary>
    public sealed class AsyncRunner : RoutineExecutorOwner
    {
        private HashSet<RoutineExecutor> m_hashSet;
        private bool m_locked;

        /// <summary>
        /// Active tasks count.
        /// </summary>
        public int TasksCount
        {
            get { return m_hashSet.Count; }
        }

        public AsyncRunner()
        {
            m_hashSet = new HashSet<RoutineExecutor>();
        }

        /// <summary>
        /// Returns array of active tasks.
        /// </summary>
        public TaskInfo[] GetActiveTasks()
        {
            return m_hashSet.Select(itm => itm.GetTask()).ToArray();
        }

        /// <summary>
        /// The same as MonoBehaviour's StartCoroutine.
        /// </summary>
        public TaskInfo StartAsync(IEnumerator routine)
        {
            return f_getRunner().RunAsync(routine);
        }

        /// <summary>
        /// Runs a referenced function after delay.
        /// </summary>
        public TaskInfo RunDelayed(float time, Action run, bool scaledTime = true)
        {
            return f_getRunner().RunAsync(Script.RunDelayedRoutine(time, run, scaledTime));
        }

        /// <summary>
        /// Runs a referenced function when <paramref name="condition"/> is true.
        /// </summary>
        public TaskInfo RunByCondition(Func<bool> condition, Action run)
        {
            return f_getRunner().RunAsync(Script.RunByConditionRoutine(condition, run));
        }

        /// <summary>
        /// Runs a referenced function on the next frame.
        /// </summary>
        public TaskInfo RunNextFrame(Action run)
        {
            return f_getRunner().RunAsync(Script.RunAfterFramesRoutine(1, run));
        }

        /// <summary>
        /// Runs a referenced function after specified frames count.
        /// </summary>
        public TaskInfo RunAfterFrames(int frames, Action run)
        {
            return f_getRunner().RunAsync(Script.RunAfterFramesRoutine(frames, run));
        }

        /// <summary>
        /// Runs a referenced function each frame while <paramref name="condition"/> is true.
        /// </summary>
        public TaskInfo RunWhile(Func<bool> condition, Action run)
        {
            return f_getRunner().RunAsync(Script.RunWhileRoutine(condition, run));
        }

        /// <summary>
        /// Stops the specified task.
        /// </summary>
        public void Stop(TaskInfo taskInfo)
        {
            if (taskInfo.IsAlive && m_hashSet.Remove(taskInfo.Runner))
            {
                m_locked = true;
                taskInfo.Runner.Stop();
                m_locked = false;
            }
        }

        /// <summary>
        /// Stops all tasks runned by this instance of the AsyncRunner.
        /// </summary>
        public void StopAll()
        {
            m_locked = true;

            foreach (var item in m_hashSet)
            {
                item.Stop();
            }
            m_hashSet.Clear();

            m_locked = false;
        }

        // - - //

        private RoutineExecutor f_getRunner()
        {
            RoutineExecutor r = AsyncStuffPool.GetExecutor();
            r.SetUp(this);
            m_hashSet.Add(r);
            return r;
        }

        void RoutineExecutorOwner.RemoveExecutor(RoutineExecutor runner)
        {
            if (m_locked) { return; }

            m_hashSet.Remove(runner);
        }
    }
}
