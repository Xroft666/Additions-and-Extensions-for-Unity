﻿using System;

namespace Hargrim.Scripts
{
    [AttributeUsage(AttributeTargets.Class)]
    public abstract class CreateInstanceAttribute : Attribute
    {
        public abstract object Create();
    }
}
