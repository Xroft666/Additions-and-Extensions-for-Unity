﻿using System;
using UnityEngine;

namespace Hargrim.Scripts
{
    /// <summary>
    /// Represents implementation of ScriptableObject singleton with lazy initialization.
    /// </summary>
    public abstract class SOSingleton<T> : ScriptableObject where T : SOSingleton<T>
    {
        private static T m_inst;

        /// <summary>
        /// Static instance of SOSingleton`1.
        /// </summary>
        public static T I
        {
            get
            {
                if (m_inst == null)
                {
                    Attribute a = Attribute.GetCustomAttribute(typeof(T), typeof(CreateInstanceAttribute), true);
                    m_inst = a != null ? (a as CreateInstanceAttribute).Create() as T : CreateInstance<T>();
                }
                return m_inst;
            }
        }

        /// <summary>
        /// Returns true if the instance is not null.
        /// </summary>
        public static bool Exists
        {
            get { return m_inst != null; }
        }

        private void OnDestroy()
        {
            m_inst = null;
            Dispose();
        }

        /// <summary>
        /// Use it instead of OnDestroy.
        /// </summary>
        protected abstract void Dispose();
    }
}
