﻿namespace Hargrim.Scripts
{
    /// <summary>
    /// Represents implementation of MonoBehaviour singleton. It has no dynamic creation of an instance.
    /// It should be saved in scene or should be created manually in runtime.
    /// </summary>
    public abstract class SingleScript<T> : Script where T : SingleScript<T>
    {
        private static T m_inst;

        /// <summary>
        /// Static instance of SingleScript`1.
        /// </summary>
        public static T I
        {
            get
            {
                if (m_inst == null)
                {
                    m_inst = FindObjectOfType<T>();
                    if (m_inst == null)
                        throw new ObjectNotFoundException(string.Format("There is no any instance of {0}.", typeof(T).Name));
                    m_inst.Construct();
                }

                return m_inst;
            }
        }

        /// <summary>
        /// Returns true if the instance is not null.
        /// </summary>
        public static bool Exists
        {
            get { return m_inst != null; }
        }

        private void Awake()
        {
            if (m_inst == null)
            {
                Construct();
                m_inst = this as T;
            }
        }

        private void OnDestroy()
        {
            m_inst = null;
            Dispose();
        }

        /// <summary>
        /// Used it instead of Awake.
        /// </summary>
        protected abstract void Construct();

        /// <summary>
        /// Used it instead of OnDestroy.
        /// </summary>
        protected abstract void Dispose();
    }
}
