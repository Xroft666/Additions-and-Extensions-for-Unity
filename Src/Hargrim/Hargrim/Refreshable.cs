﻿namespace Hargrim
{
    public interface Refreshable
    {
        void Refresh();
    }
}
